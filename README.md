# CRYPT
#### Video Demo:  https://www.youtube.com/watch?v=1HrtPQk14Ds
#### Description: This is a script made for encrypting/decrypting files/folders

## I - Full Description

Okay, there's a few things to discuss here,

I made this script to keep files/folders safe, there's a process when the script is executing, that's exactly what we're going to discuss in this part.

Okay so first things first, the "opening" of the script.
When you're launching the script there are some checks, the most obvious is if you run as root, if not the program quit and it ask you to run as root. The second (most lengthy), check if the package are installed, but it's pretty fast, i promise, no issue on the speed side of this part, but you'll need an internet connection since it will check for packages (or you can comment that part if you don't have one and the packages are installed, it's up to you ;) ).

there are 3 steps before it run most of the program, and another, way after the 3 are executed
1. Getting input for a source path, the path leading to your encrypted file or elements you want to encrypt.
2. Getting input for a destination path, after the process whether it's encryption or decryption, the path where the processed elements are going to be.
3. Getting input for the method of cryption, do you want to encrypt or decrypt something ?
4. The last isn't directly from the script, it's goint to be a call to GPG who's going to ask you for a password, whether you decrypt or encrypt.

As you can see, it's pretty straightforward and the manipulation of the script, is pretty fast, easy and secure !
But, some warning have to be made !
If you input a path that doesn't exist luckily for you, the program stop. But if you input, your Desktop path for example and you didn't precise anything, you're going to get all your files from the Desktop deleted and encrypted.
It's not a big problem if you remember your password, it will only be a boring process to decrypt files you didn't want to encrypt, move them to their original places etc...
But as always **BE AWARE OF WHAT YOU'RE TYPING IN THE PATHS INPUTS**
The second thing to be aware is the length of the process, it's pretty obvious but I would say it just in case:
Depending on the size of the elements you want to encrypt/decrypt, the speed of your HDD/SSD, but also the heat of your storage,
**THE PROCESS OF ENCRYPTION/DECRYPTION CAN VARY REGARDING THE HARDWARE YOU'RE USING**, so pay attention to that also !
Last thing, **REMEMBER YOUR GPG KEY** !

## II - Details

For this script, I use some package who are dependencies for this program.
I used GPG, tar and some UNIX command like basename or find.
Regarding the dependencies, I made some part of the script to check
if the computer who's running the program has these dependencies.
Throughout the execution you can see that I've made some check, to be sure that no other data (other than what you choose in your paths) should be modify, compress, delete or moved somewhere where it doesn't supposed to be for example.
All of the program (about 300 lines), is on the source.sh file, you should execute it pretty simply.
Although you can probably run this on most of the "know distro", like Debian or Ubuntu, since I used [DPKG](https://en.wikipedia.org/wiki/Dpkg) for the part where I check the dependencies. Make sure to find a way around, if you're using a distro not based on Debian.


## III - Precise details + Fixing some of the most common problems faced

For this script, you'll need root permission, because we're moving files and you really want to avoid permissions error.
You also need to use, if possible, absolute path and not relative
(check here if any problems regarding paths that you don't understand: [PATHS](https://docs.oracle.com/javase/tutorial/essential/io/path.html)
To make the script "easier" and "faster", the verbose option isn't specify, if you want some debug info just type "-x" on the top of the file next to the line:  "#!/bin/bash", don't worry for the -e option on bash, there's some If condition, so it's faster easier for debug and pretty safe regarding the various actions the script make on your computer.
When you include your path, whether it's the destination or the source,
it may sound crazy but don't forget to include the folder where your files/folders you want to modify are,
example: I have files on my desktop, I absolutely have to create a folder in which i put the file/folder I want to encrypt,
because once paths are chosen, choice of encryption made, there's no turning back.
For a clearer example:
GOOD  => /home/user/Desktop/special_folder_with_files_or_folders_i_want_to_encrypt_in_it (created to hold some element I want to encrypt or the folder where my encrypted files are already there)
NOT GOOD => /home/user/Desktop
BECAUSE the script don't know where to find precisely your elements.
And it's basically the same thing with decryption process.

FOR ENCRYPTION:
Source path: include the path you chosen + '/' + folder where you put your items
Destination path: only the path where you want to store the encrypted file

FOR DECRYPTION:
Source path: include the path you chosen + '/' + folder where your GPG file is
Destination path: same as encryption, only the path where you want your unencrypted folder to be.

DON'T FORGET TO CHMOD +X your script so he can be executed.
Okay, for this you'll probably need root, once you're in the directory of the script. You should make this command: sudo chmod +x source.sh (or the name you gave to my script). This will allow to directly execute the program like this ./source.sh . Since you need root, when executing the script you already have some elements to include like sudo!
Normal command if you previously used chmod +x => sudo ./source.sh
If not => sudo bash source.sh

Ok so GPG (the tool used for encryption/decryption in the script)
use AES-256 for the method of encryption (so even if you have lightweight file/folder you want to encrypt it's not the fastest encrypting method due to its complexity). The real problem is that your GPG KEY is going to be "save" in sort of, since the tool manage your keys and the amount of time associated to them before prompting again for a password. So if you encrypt your file and decrypt one minute later the process of decryption won't ask for the password (as you can see in the video link in top of the README).
The problem is, the script can't directly and (for a safety point of view also) change your GPG settings If you're not aware or if you simply don't want to be asked for the password each time your decrypt a document, on a short period of time. So even if it ruins the process, it's up to you. If you want some advice, you should really modify your GPG settings on your machine to be the most secure possible, so check there to see how to modify [GPG](https://unix.stackexchange.com/questions/395875/gpg-does-not-ask-for-password).