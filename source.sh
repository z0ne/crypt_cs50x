#!/bin/bash
# Good to use the -e option so if errors, the script is just going to stop

# Check for root
if [ $EUID -ne 0 ]; then
   echo "You must run the script as root." 1>&2
   exit 100
fi

# Check if package are installed
# Check for tar
REQUIRED_PKG="tar"
PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $REQUIRED_PKG|grep "install ok installed")
echo Checking for $REQUIRED_PKG: $PKG_OK
if [ "" = "$PKG_OK" ]; then
    echo "No $REQUIRED_PKG. Setting up $REQUIRED_PKG."
    sudo apt-get --yes install $REQUIRED_PKG
fi

#Check for gpg
REQUIRED_PKG="gpg"
PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $REQUIRED_PKG|grep "install ok installed")
echo Checking for $REQUIRED_PKG: $PKG_OK
if [ "" = "$PKG_OK" ]; then
    echo "No $REQUIRED_PKG. Setting up $REQUIRED_PKG."
    sudo apt-get --yes install $REQUIRED_PKG
fi

# Clear our screen to be nice and elegant.
clear

# Getting our paths
read -p "Please give us your source path : " src

# Going to that path to see if it's possible
cd $src

# If not the console return a positive number or 0 (if sucess)
if [ $? -eq 0 ]; then
        echo -e "\e[34m[+] The source path exist.\e[0m"
    else
        echo -e "\e[31m[-] The source path you gave doesn't exist, try in absolute path maybe ?\e[0m"

        exit
    fi

# Returning to home.
cd

# Getting the second path
read -p "Please give us your destination path: " dest

# Try again the path given
cd $dest

# Checking for error after the previous command
if [ $? -eq 0 ]; then
        echo -e "\e[34m[+] The destination path exist.\e[0m"
    else
        echo -e "\e[31m[-] The destination path you gave doesn't exist, try in absolute path maybe ?\e[0m"

        exit
    fi

# Going back to home
cd

# Creating the filename
filename=$(basename "$src")

# Function to encrypt, jump there if the user want to encrypt something
function encrypt {
    # First go to the source
    cd $src

    # Create a filename with the _crypt particule
    crypt=$filename"_crypt"

    # Creating a folder so we operate our command in some sort of sandbox
    #(we don't want to modify other file/foler right ?)
    mkdir -p $crypt

    # Start by moving everything we see in this folder
    #(using the 2>/dev/null so we aren't in some sort of verbose mod)
    mv -f * $crypt 2>/dev/null

    # Creating a filename for the tar part
    crypt_tar=$crypt".tar.gz"

    # Compress our folder through the 2 names we create
    tar czf $crypt_tar $crypt

    # Check for error when cmpressing
    if [ $? -eq 0 ]; then
        echo -e "\e[34m[+] The project has been compressed.\e[0m"
    else
        echo -e "\e[31m[-] The project has not been compressed.\e[0m"

        exit
    fi

    # Gpg (encrypt) our file with the -c
    gpg -c $crypt_tar

    # Check for error
    if [ $? -eq 0 ]; then
        echo -e "\e[34m[+] The project has been encrypted.\e[0m"
    else
        echo -e "\e[31m[-] The project has not been encrypted.\e[0m"

        exit
    fi

    # Creating a name for gpg part
    crypt_tar_gpg=$crypt_tar".gpg"

    # Creating the path/folder name for the crypt filename
    crypt_path_folder=$dest'/'$crypt

    # Creating the folder itself
    mkdir -p $crypt_path_folder

    # Moving our encryted file to the destination the user specified
    mv -f $crypt_tar_gpg $crypt_path_folder

    # Deleting everything else (tar file + gpg file)
    rm -rf *

    # Going upper in the file hierarchy
    cd ..

    # To delete our "sandbox" folder
    rmdir $filename

    # Going home
    cd

    # Check if we find the encrypted file in the destination path prtovided
    find $crypt_path_folder'/'$crypt_tar_gpg 2>/dev/null

    # If so our script is merely over !
    if [ $? -eq 0 ]; then
        echo -e "\e[34m[+] The project has been found in this destination directory.\e[0m"
    else
        echo -e "\e[31m[-] The project has not been found.\e[0m"

        exit
    fi
}

function decrypt {

    # Go to the source, as usual
    cd $src

    # Creating the name for tar filename
    crypt_tar=$filename".tar.gz"

    # Creating the name for gpg filename
    crypt_tar_gpg=$crypt_tar".gpg"

    # Start by gpg our file, so we can decrypt it with the -o argument
    # And the two filename we created before
    gpg -o $crypt_tar -d $crypt_tar_gpg

    # if successful we continue.
    if [ $? -eq 0 ]; then
        echo -e "\e[34m[+] The project has been decrypted.\e[0m"
    else
        echo -e "\e[31m[-] The project has not been decrypted.\e[0m"

        exit
    fi

    # Decompress our element
    tar xzf $crypt_tar

    # If problem, again we check
    if [ $? -eq 0 ]; then
        echo -e "\e[34m[+] The project has been decompressed.\e[0m"
    else
        echo -e "\e[31m[-] The project has not been decompressed.\e[0m"

        exit
    fi

    # Creating a new name for the folder where we store our info
    newfolder="${filename%_*}"

    # Change the name, so it's now the name without the _crypt particule
    mv $filename $newfolder

    # Move our unencrypted folder to the destination path
    mv $newfolder $dest

    # Deleting everything in the file
    rm -rf *

    # Again we're going to go back to delete the folder itself
    cd ..

    # Deleting the folder
    rmdir $filename

    # Going to our destination path
    cd $dest

    # Creating user variable
    user=$(ls /home)

    # To change ownership of the file
    # (Since it was root before, so now user have control on it)
    chown -R $user:$user $newfolder

    # Getting to home
    cd

    # Check if everything is in place, the folder in the destination path, etc
    find $dest'/'$newfolder 2>/dev/null

    # If so we're done !
	if [ $? -eq 0 ]; then
		echo -e "\e[34m[+] The project has been found in this directory and is now ready to be modified.\e[0m"
	else
		echo -e "\e[31m[-] The project has not been found, so it is not ready to be modified.\e[0m"

        exit
	fi
}

function main {
    # First we're getting input to check what the user want to do
    read -p "What do you want? (encrypt/decrypt): " choice

    # After we make a function call to know if the user want's to encrypt or decrypt
    # Also if something else was provided we call again our main function
    # So it's recursive and we don't have to quit the script type our paths again, etc....
	if [ "$choice" = "encrypt" ]
    then
        encrypt

    elif [ "$choice" = "decrypt" ]
    then
	    decrypt
    else
		echo "Error, please retype something."

        main
	fi
}

# We calling our main to really start the program, after we check our dependencies, and getting our paths
main
